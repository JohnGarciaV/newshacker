/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.infraestructure.entry.points.controller;

import com.apply.digital.news.application.handler.AuthenticateHandler;
import com.apply.digital.news.application.handler.StoryHandler;
import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.model.dto.UserDto;
import com.apply.digital.news.domain.model.response.data.StoryResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@RequestMapping("/story")
public class StoryController {

    private final StoryHandler storyHandler;
    private final AuthenticateHandler authenticateHandler;

    public StoryController(final StoryHandler storyHandler, AuthenticateHandler authenticateHandler) {
        this.storyHandler = storyHandler;
        this.authenticateHandler = authenticateHandler;
    }

    private StoryHandler getStoryHandler(){
        return storyHandler;
    }
    private AuthenticateHandler getAuthenticateHandler(){
        return authenticateHandler;
    }

    @PostMapping("/login")
    public ResponseEntity<UserDto> login(@RequestParam String username,
                                         @RequestParam String password){

        UserDto user = getAuthenticateHandler().validateUser(username, password);
        if(user == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/filter/{page}")
    public ResponseEntity<StoryResponse> getStoriesByFilter(@Nullable @RequestParam String author,
                                                              @Nullable @RequestParam String tags,
                                                              @Nullable @RequestParam String title,
                                                              @Nullable @RequestParam String month,
                                                              @PathVariable int page){
        StoryResponse response = getStoryHandler().findAllStoryByFilter(author, tags, title, month, page);
        if(response.getTotalElements() == 0){
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<StoryDto> deleteStory(@PathVariable Long id){
        StoryDto story = getStoryHandler().deleteStory(id);
        if(story == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
       return new ResponseEntity<>(story, HttpStatus.OK);
    }
}
