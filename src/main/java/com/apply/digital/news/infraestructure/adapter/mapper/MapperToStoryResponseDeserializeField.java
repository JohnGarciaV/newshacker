/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.infraestructure.adapter.mapper;

import com.apply.digital.news.domain.model.response.news.story.HighlightResult;
import com.apply.digital.news.domain.model.response.news.story.DataStory;
import com.apply.digital.news.infraestructure.adapter.entity.Story;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.Optional;

@Slf4j
public class MapperToStoryResponseDeserializeField {

    public DataStory mapper(Story story){
        try {
            HighlightResult highlightResult = new HighlightResult();
            if(story.getHighlightResult() != null && !story.getHighlightResult().isEmpty()){
                 highlightResult = new Gson().fromJson(story.getHighlightResult(), HighlightResult.class);
            }
            DataStory storyResponse = new DataStory();
            storyResponse.setStory_id(story.getStoryId());
            storyResponse.setCreated_at(story.getCreatedAt());
            storyResponse.setStory_title(story.getTitle());
            storyResponse.setUrl(Optional.of(story.getUrl()));
            storyResponse.setAuthor(story.getAuthor());
            storyResponse.setPoints(Optional.of(story.getPoints()));
            storyResponse.setStory_text(Optional.of(story.getStoryText()));
            storyResponse.setComment_text(story.getCommentText());
            storyResponse.setNum_comments(Optional.of(Objects.isNull(story.getNumComments())? 0L: story.getNumComments()));
            storyResponse.setStory_title( story.getStoryTitle());
            storyResponse.setStory_url(story.getStoryUrl());
            storyResponse.setParent_id(story.getParentId());
            storyResponse.setCreated_at_i(story.getCreatedAtI());
            storyResponse.set_tags(story.getTags());
            storyResponse.setObjectID(story.getObjectID());
            storyResponse.set_highlightResult(highlightResult);
            return storyResponse;
        }catch (Exception err){
            log.info("Error MapperToStoryDtoDeserializeField: ", err.toString());
            return null;
        }
    }
}
