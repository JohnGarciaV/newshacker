/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.infraestructure.adapter.mapper;

import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.infraestructure.adapter.entity.Story;

public class StoryMapperToStoryDto {

    public StoryDto mapper(Story story){
        return new StoryDto(
                story.getStoryId(),
                story.getCreatedAt(),
                story.getTitle(),
                story.getUrl(),
                story.getAuthor(),
                story.getPoints(),
                story.getStoryText(),
                story.getCommentText(),
                story.getNumComments(),
                story.getStoryTitle(),
                story.getStoryUrl(),
                story.getParentId(),
                story.getCreatedAtI(),
                story.getTags(),
                story.getObjectID(),
                story.getHighlightResult(),
                story.isState()
        );
    }
}
