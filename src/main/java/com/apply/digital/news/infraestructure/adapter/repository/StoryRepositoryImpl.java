/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.infraestructure.adapter.repository;

import com.apply.digital.news.application.constant.Constant;
import com.apply.digital.news.infraestructure.adapter.entity.Story;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class StoryRepositoryImpl {

    @PersistenceContext
    private EntityManager entityManager;

    public Page<Story> getDataFilterAndPagination(HashMap<String, Object> conditions, Pageable pageable)
    {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Story> query= cb.createQuery(Story.class);
        Root<Story> root = query.from(Story.class);

        List<Predicate> predicates = new ArrayList<>();
        conditions.forEach((field,value) ->
        {
            switch (field)
            {
                case Constant.FIELD_AUTHOR:
                    predicates.add(cb.equal(root.get(field), value));
                    break;
                case Constant.FIELD_TAGS:
                    predicates.add(cb.like(root.get(field).as(String.class),"%"+value+"%"));
                    break;
                case Constant.FIELD_TITLE:
                    predicates.add(cb.like(root.get(field), "%"+value+"%"));
                    break;

            }
        });
        predicates.add(cb.equal (root.get(Constant.FIELD_STATE), (boolean) true));
        query.select(root).where(cb.and( predicates.toArray(new Predicate[predicates.size()])));
        List<Story> listStories = entityManager.createQuery(query)
                .setFirstResult((int) pageable.getOffset())
                .setMaxResults(pageable.getPageSize()).getResultList();
        List<Story> totalStories = entityManager.createQuery(query).getResultList();
        Integer total = totalStories.size();
        long count = total.longValue();

        return new PageImpl<>(listStories, pageable, count);
    }
}
