/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.infraestructure.adapter.entity;

import com.apply.digital.news.infraestructure.adapter.data.StringListConverter;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Convert;

import java.util.List;

@Entity
@Table(name = "story")
public class Story {

    @Id
    @Column(name = "story_id")
    private Long storyId;

    @Column(name = "created_at")
    private String createdAt;

    @Column(name = "title")
    private String title;

    @Column(name = "url")
    private String url;

    @Column(name = "author")
    private String author;

    @Column(name = "points")
    private String points;

    @Column(name = "story_text")
    private String storyText;

    @Column(name = "comment_text", columnDefinition="TEXT")
    private String commentText;

    @Column(name = "num_comments")
    private Long numComments;

    @Column(name = "story_title", columnDefinition="TEXT")
    private String storyTitle;

    @Column(name = "story_url")
    private String storyUrl;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "created_at_i")
    private Long createdAtI;

    @Convert(converter = StringListConverter.class)
    @Column(name = "tags")
    private List<String> tags;

    @Column(name = "object_id")
    private String objectID;

    @Column(name = "high_light_result", columnDefinition="TEXT")
    private String highlightResult;

    @Column(name = "state")
    private boolean state;

    public Long getStoryId() {
        return storyId;
    }

    public void setStoryId(Long storyId) {
        this.storyId = storyId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getStoryText() {
        return storyText;
    }

    public void setStoryText(String storyText) {
        this.storyText = storyText;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Long getNumComments() {
        return numComments;
    }

    public void setNumComments(Long numComments) {
        this.numComments = numComments;
    }

    public String getStoryTitle() {
        return storyTitle;
    }

    public void setStoryTitle(String storyTitle) {
        this.storyTitle = storyTitle;
    }

    public String getStoryUrl() {
        return storyUrl;
    }

    public void setStoryUrl(String storyUrl) {
        this.storyUrl = storyUrl;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getCreatedAtI() {
        return createdAtI;
    }

    public void setCreatedAtI(Long createdAtI) {
        this.createdAtI = createdAtI;
    }

    public List<String> getTags() {
        return tags;
    }
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getObjectID() {
        return objectID;
    }

    public void setObjectID(String objectID) {
        this.objectID = objectID;
    }

    public String getHighlightResult() {
        return highlightResult;
    }

    public void setHighlightResult(String highlightResult) {
        this.highlightResult = highlightResult;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
