/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.infraestructure.adapter.consumer.news;

import com.apply.digital.news.application.constant.Constant;
import com.apply.digital.news.domain.model.response.news.story.Stories;
import com.apply.digital.news.domain.port.consumer.NewsHackerConsumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class NewsHackerClientConsumer implements NewsHackerConsumer {

    private final RestTemplate clientRestTemplate;

    public NewsHackerClientConsumer(RestTemplate clientRestTemplate) {
        this.clientRestTemplate = clientRestTemplate;
    }

    private RestTemplate getClientRestTemplate(){
        return clientRestTemplate;
    }

    @Override
    public Stories getNewsStories() {
        ResponseEntity<Stories> responseNewStories = getClientRestTemplate().getForEntity(Constant.HOST_NEW_STORY, Stories.class);

        if(responseNewStories.getStatusCodeValue() == 200){
            return responseNewStories.getBody();
        }
        log.info("Status code get new story: "+responseNewStories.getStatusCodeValue());
        return null;
    }
}
