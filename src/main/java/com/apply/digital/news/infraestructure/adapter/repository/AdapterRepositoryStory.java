/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.infraestructure.adapter.repository;

import com.apply.digital.news.application.constant.Constant;
import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.model.response.data.StoryResponse;
import com.apply.digital.news.domain.model.response.news.story.DataStory;
import com.apply.digital.news.domain.port.repository.StoryRepository;
import com.apply.digital.news.infraestructure.adapter.entity.Story;
import com.apply.digital.news.infraestructure.adapter.mapper.MapperToStoryResponseDeserializeField;
import com.apply.digital.news.infraestructure.adapter.mapper.StoryDtoMapperToStory;
import com.apply.digital.news.infraestructure.adapter.mapper.StoryMapperToStoryDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Repository
public class AdapterRepositoryStory implements StoryRepository {

    private final StoryDao storyDao;
    private final StoryMapperToStoryDto storyDtoMapper;
    private final StoryDtoMapperToStory storyMapper;
    private final MapperToStoryResponseDeserializeField mapperStoryResponse;
    private final StoryRepositoryImpl storyRepositoryImpl;

    public AdapterRepositoryStory(StoryDao repositoryStory, StoryMapperToStoryDto storyDtoMapper,
                                  StoryDtoMapperToStory storyMapper,
                                  MapperToStoryResponseDeserializeField mapperStoryResponse,
                                  StoryRepositoryImpl storyRepositoryImpl) {
        this.storyDao = repositoryStory;
        this.storyMapper = storyMapper;
        this.storyDtoMapper = storyDtoMapper;
        this.mapperStoryResponse = mapperStoryResponse;
        this.storyRepositoryImpl = storyRepositoryImpl;
    }

    private StoryMapperToStoryDto getStoryMapper(){
        return storyDtoMapper;
    }

    private StoryDtoMapperToStory getStoryDtoMapper(){
        return storyMapper;
    }

    private MapperToStoryResponseDeserializeField getMapperStoryResponse(){
        return mapperStoryResponse;
    }

    private StoryRepositoryImpl getStoryRepositoryImpl(){
        return storyRepositoryImpl;
    }

    @Override
    public boolean saveStory(StoryDto storyDto) {
        try{
            storyDao.save(getStoryDtoMapper().mapper(storyDto));
            return true;
        }catch (Exception ex){
            log.info("Error save story");
            return false;
        }
    }

    @Override
    public StoryDto findById(Long id) {
        try{
            Optional<Story> story = storyDao.findById(id);
            if(story.isPresent()){
                return getStoryMapper().mapper(story.get());
            }
        }catch (Exception ex){
            log.info("Error findById story", ex);
            return null;
        }
        return null;
    }

    @Override
    public StoryResponse findByFilters(HashMap<String, Object> conditions, Pageable pageable) {
        try{
            Page<Story> dataFilter = getStoryRepositoryImpl().getDataFilterAndPagination(conditions, pageable);
            StoryResponse storyResponse = new StoryResponse();

            if(Objects.nonNull(dataFilter)) {
                List<DataStory> listStory = new ArrayList<>();

                if(conditions.containsKey(Constant.FIELD_CREATED_AT)) {
                     listStory = dataFilter.stream()
                            .map(item -> getMapperStoryResponse().mapper(item))
                            .filter(item -> filterCreateAt(item, (String) conditions.get(Constant.FIELD_CREATED_AT)))
                            .collect(Collectors.toList());
                }else{
                    listStory = dataFilter.stream()
                            .map(item -> getMapperStoryResponse().mapper(item))
                            .collect(Collectors.toList());
                }

                storyResponse.setStories(listStory);
                storyResponse.setSize(dataFilter.getSize());
                storyResponse.setPageCurrent(pageable.getPageNumber());
                storyResponse.setTotalPages(dataFilter.getTotalPages());
                storyResponse.setTotalElements(listStory.size());
            }else{
                storyResponse.setSize(0);
                storyResponse.setPageCurrent(pageable.getPageNumber());
                storyResponse.setTotalPages(0);
                storyResponse.setTotalElements(0L);
            }
            return storyResponse;

        }catch (Exception ex){
            log.info("Error findByFilter story", ex);
            return null;
        }
    }

    private Boolean filterCreateAt(DataStory data, String month){
        try {
            OffsetDateTime date = OffsetDateTime.parse( data.getCreated_at());
            return month.equals( date.getMonth().name());
        }catch (Exception ex){
            log.info("Error converter date");
            return false;
        }
    }
}
