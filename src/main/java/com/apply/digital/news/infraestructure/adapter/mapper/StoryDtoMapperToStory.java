/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.infraestructure.adapter.mapper;

import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.infraestructure.adapter.entity.Story;

public class StoryDtoMapperToStory {

    public Story mapper(StoryDto story){
        Story storyEntity = new Story();
        storyEntity.setStoryId(story.getStoryId());
        storyEntity.setCreatedAt(story.getCreatedAt());
        storyEntity.setTitle(story.getTitle());
        storyEntity.setUrl(story.getUrl());
        storyEntity.setAuthor( story.getAuthor());
        storyEntity.setPoints(story.getPoints());
        storyEntity.setStoryText(story.getStoryText());
        storyEntity.setCommentText(story.getCommentText());
        storyEntity.setNumComments(story.getNumComments());
        storyEntity.setStoryTitle(story.getStoryTitle());
        storyEntity.setStoryUrl(story.getStoryUrl());
        storyEntity.setParentId(story.getParentId());
        storyEntity.setCreatedAtI(story.getCreatedAtI());
        storyEntity.setTags(story.getTags());
        storyEntity.setObjectID(story.getObjectID());
        storyEntity.setHighlightResult(story.getHighlightResult());
        storyEntity.setState(story.isState());
        return storyEntity;
    }
}
