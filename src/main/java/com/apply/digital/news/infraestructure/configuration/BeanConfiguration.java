/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.infraestructure.configuration;

import com.apply.digital.news.application.factory.ConditionsFactory;
import com.apply.digital.news.application.factory.StoryFactory;
import com.apply.digital.news.application.handler.AuthenticateHandler;
import com.apply.digital.news.application.handler.NewsHackerHandler;
import com.apply.digital.news.application.handler.StoryHandler;
import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.port.consumer.NewsHackerConsumer;
import com.apply.digital.news.domain.port.repository.StoryRepository;
import com.apply.digital.news.domain.port.security.TokenSecurity;
import com.apply.digital.news.domain.service.NewsHackerService;
import com.apply.digital.news.domain.service.StoryService;
import com.apply.digital.news.infraestructure.adapter.consumer.news.NewsHackerClientConsumer;
import com.apply.digital.news.infraestructure.adapter.mapper.MapperToStoryResponseDeserializeField;
import com.apply.digital.news.infraestructure.adapter.mapper.StoryDtoMapperToStory;
import com.apply.digital.news.infraestructure.adapter.mapper.StoryMapperToStoryDto;
import com.apply.digital.news.infraestructure.security.TokenJWT;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class BeanConfiguration {

    @Bean(name = "newsHackerClient")
    public RestTemplate restTemplate() {
        return new RestTemplate(
                new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
    }

    @Bean
    public NewsHackerClientConsumer newsHackerConsumer(RestTemplate restTemplate){
        return new NewsHackerClientConsumer(restTemplate);
    }

    @Bean
    public NewsHackerService newsHackerService(NewsHackerConsumer newsHackerConsumer){
        return new NewsHackerService(newsHackerConsumer);
    }

    @Bean
    public StoryFactory storyFactory(){
        return new StoryFactory();
    }

    @Bean
    public ConditionsFactory conditionsFactory(){
        return new ConditionsFactory();
    }

    @Bean
    public StoryDto storyDto(){
        return new StoryDto();
    }

    @Bean
    public StoryMapperToStoryDto storyMapperToStoryDto(){
        return new StoryMapperToStoryDto();
    }

    @Bean
    public StoryDtoMapperToStory storyDtoMapperToStory(){
        return new StoryDtoMapperToStory();
    }

    @Bean
    public MapperToStoryResponseDeserializeField mapperStoryResponse(){
        return new MapperToStoryResponseDeserializeField();
    }

    @Bean
    public TokenJWT tokenJWT(){
        return new TokenJWT();
    }

    @Bean
    public StoryService storyService(StoryRepository storyDao){
        return new StoryService(storyDao);
    }

    @Bean
    public NewsHackerHandler newsHackerHandler(NewsHackerService newsHackerService, StoryService storyService,
                                               StoryFactory storyFactory){
        return new NewsHackerHandler(newsHackerService, storyService, storyFactory);
    }

    @Bean
    public AuthenticateHandler authenticateHandler(TokenSecurity tokenSecurity){
        return new AuthenticateHandler(tokenSecurity);
    }

    @Bean
    public StoryHandler storyHandler(StoryService storyService,
                                     ConditionsFactory conditionsFactory){
        return new StoryHandler(storyService, conditionsFactory);
    }

}
