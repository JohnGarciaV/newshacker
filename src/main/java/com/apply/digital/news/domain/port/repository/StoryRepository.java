/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.domain.port.repository;

import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.model.response.data.StoryResponse;
import org.springframework.data.domain.Pageable;

import java.util.HashMap;

public interface StoryRepository {

    boolean saveStory(StoryDto storyDto);
    StoryDto findById(Long id);
    StoryResponse findByFilters(HashMap<String, Object> conditions, Pageable pageable);
}
