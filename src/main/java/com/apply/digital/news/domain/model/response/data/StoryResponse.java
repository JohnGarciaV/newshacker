package com.apply.digital.news.domain.model.response.data;

import com.apply.digital.news.domain.model.response.news.story.DataStory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StoryResponse {

    private List<DataStory> stories;
    private int size;
    private int pageCurrent;
    private int totalPages;
    private long totalElements;
}
