/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.domain.service;

import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.model.response.data.StoryResponse;
import com.apply.digital.news.domain.port.repository.StoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;

import java.util.HashMap;

@Slf4j
public class StoryService {

    private final StoryRepository storyDao;

    public StoryService(final StoryRepository storyDao) {
        this.storyDao = storyDao;
    }

    private StoryRepository getStoryDao(){
        return storyDao;
    }

    public boolean saveStory(StoryDto storyDto){
        return getStoryDao().saveStory(storyDto);
    }

    public StoryDto findStoryById(Long id){
        return getStoryDao().findById(id);
    }

    public StoryResponse findByFilters(HashMap<String, Object> conditions, Pageable pageable){
        return getStoryDao().findByFilters(conditions, pageable);
    }
}
