/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.domain.model.response.news.story;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Stories {

    private List<DataStory> hits;
    private long nbHits;
    private int page;
    private int nbPages;
    private int hitsPerPage;
    private boolean exhaustiveNbHits;
    private boolean exhaustiveTypo;
    private Exhaustive exhaustiveT;
    private String query;
    private String params;
    private int processingTimeMS;
    private ProcessingTimingsMS processingTimingsMS;
    private int serverTimeMS;
}
