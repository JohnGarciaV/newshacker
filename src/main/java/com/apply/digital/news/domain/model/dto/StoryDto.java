/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.domain.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
public class StoryDto {

    private Long storyId;
    private String createdAt;
    private String title;
    private String url;
    private String author;
    private String points;
    private String storyText;
    private String commentText;
    private Long numComments;
    private String storyTitle;
    private String storyUrl;
    private Long parentId;
    private Long createdAtI;
    private List<String> tags;
    private String objectID;
    private String highlightResult;
    @Setter @Getter private boolean state;

    public StoryDto(){};

    public StoryDto(
            final Long storyId,
            final String createdAt,
            final String title,
            final String url,
            final String author,
            final String points,
            final String storyText,
            final String commentText,
            final Long numComments,
            final String storyTitle,
            final String storyUrl,
            final Long parentId,
            final Long createdAtI,
            final List<String> tags,
            final String objectID,
            final String highlightResult,
            final boolean state) {


        this.storyId = storyId;
        this.createdAt = createdAt;
        this.title = title;
        this.url = url;
        this.author = author;
        this.points = points;
        this.storyText = storyText;
        this.commentText = commentText;
        this.numComments = numComments;
        this.storyTitle = storyTitle;
        this.storyUrl = storyUrl;
        this.parentId = parentId;
        this.createdAtI = createdAtI;
        this.tags = tags;
        this.objectID = objectID;
        this.highlightResult = highlightResult;
        this.state = state;
    }
}
