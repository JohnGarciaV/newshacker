/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.domain.model.response.news.story;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Optional;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DataStory {

    private String created_at;
    private Optional<String> title;
    private Optional<String> url;
    private String author;
    private Optional<String> points;
    private Optional<String> story_text;
    private String comment_text;
    private Optional<Long> num_comments;
    private long story_id;
    private String story_title;
    private String story_url;
    private long parent_id;
    private long created_at_i;
    private List<String> _tags;
    private String objectID;
    private HighlightResult _highlightResult;

}
