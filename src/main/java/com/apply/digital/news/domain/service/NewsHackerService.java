/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.domain.service;

import com.apply.digital.news.domain.model.response.news.story.Stories;
import com.apply.digital.news.domain.port.consumer.NewsHackerConsumer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NewsHackerService {

    private final NewsHackerConsumer newsHackerClient;

    public NewsHackerService(final NewsHackerConsumer newsHackerClient) {
        this.newsHackerClient = newsHackerClient;
    }

    private NewsHackerConsumer getNewsHackerClient(){
        return newsHackerClient;
    }

    public Stories execute(){
        return getNewsHackerClient().getNewsStories();
    }
}
