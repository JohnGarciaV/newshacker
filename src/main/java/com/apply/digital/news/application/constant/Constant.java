/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.application.constant;

public class Constant {
    private Constant() {}

    public static final String MESSAGE_SAVE = "SAVE STORY ID: ";
    public static final String MESSAGE_ERROR_SAVE = "ERROR SAVE: ";
    public static final String MESSAGE_ALREADY_REGISTERED = "ALREADY REGISTERED ID STORY: ";
    public static final String MESSAGE_ERROR_JSON_CONVERT = "ERROR JSON CONVERT TO STRING: ";
    public static final int ELEMENTS= 5;
    public static final String MESSAGE_ERROR_MONTH = "ERROR MONTH NOT FOUND";
    public static final String MESSAGE_ERROR_CLIENT = "ERROR GET NEW STORY";
    public static final String FIELD_AUTHOR = "author";
    public static final String FIELD_TAGS = "tags";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_STATE = "state";
    public static final String FIELD_CREATED_AT = "created_at";
    public static final String HOST_NEW_STORY = "https://hn.algolia.com/api/v1/search_by_date?query=java";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String PREFIX_TOKEN = "Bearer ";
    public static final int EXPIRATION_TOKEN = 300000;
}

