/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.application.factory;

import com.apply.digital.news.application.constant.Constant;
import com.apply.digital.news.application.exception.MonthValueException;
import com.apply.digital.news.application.util.Month;
import java.util.HashMap;
import java.util.Locale;

public class ConditionsFactory {

    public HashMap<String, Object> getConditions(String author, String tags, String title, String month){
        HashMap<String, Object> conditions = new HashMap<>();
        if(author != null && !author.isEmpty()){
          conditions.put("author", author);
        }
        if(tags != null && !tags.isEmpty()){
          conditions.put("tags", tags);
        }
        if(title != null && !title.isEmpty()){
           conditions.put("title", title.toLowerCase(Locale.ROOT));
        }
        if(month != null && !month.isEmpty()){
            if(Month.isMonth(month.trim().toUpperCase())) {
                month = Month.findByNameSpanish(month.trim().toUpperCase());
                conditions.put(Constant.FIELD_CREATED_AT, month);
            }else{
                throw new MonthValueException(Constant.MESSAGE_ERROR_MONTH);
            }
        }
        return conditions;
    }
}
