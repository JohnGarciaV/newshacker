/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.application.factory;

import com.apply.digital.news.domain.model.dto.StoryDto;

import java.util.List;

public class StoryFactory {

    public StoryDto create(
            final Long storyId,
            final String createdAt,
            final String title,
            final String url,
            final String author,
            final String points,
            final String storyText,
            final String commentText,
            final Long numComments,
            final String storyTitle,
            final String storyUrl,
            final Long parentId,
            final Long createdAtI,
            final List<String> tags,
            final String objectID,
            final String highlightResult,
            final boolean state) {
        return new StoryDto(
                storyId,
                createdAt,
                title,
                url,
                author,
                points,
                storyText,
                commentText,
                numComments,
                 storyTitle,
                storyUrl,
                parentId,
                createdAtI,
                tags,
                objectID,
                highlightResult,
                state);
    }
}
