/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.application.handler;

import com.apply.digital.news.application.constant.Constant;
import com.apply.digital.news.application.exception.ClientException;
import com.apply.digital.news.application.factory.StoryFactory;
import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.model.response.news.story.Stories;
import com.apply.digital.news.domain.model.response.news.story.DataStory;
import com.apply.digital.news.domain.service.NewsHackerService;
import com.apply.digital.news.domain.service.StoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
public class NewsHackerHandler {

    private final NewsHackerService newsHackerService;
    private final StoryService storyService;
    private final StoryFactory storyFactory;


    public NewsHackerHandler(NewsHackerService newsHackerService, StoryService storyService, StoryFactory storyFactory) {
        this.newsHackerService = newsHackerService;
        this.storyService = storyService;
        this.storyFactory = storyFactory;
    }

    private NewsHackerService getNewsHackerService(){
        return newsHackerService;
    }

    private StoryService getStoryService(){
        return storyService;
    }

    private StoryFactory getStoryFactory(){
        return storyFactory;
    }

    @Scheduled(cron = "0 0 */1 * * *") 
    public void execute(){
        Stories storyResponse = getNewsHackerService().execute();
        if(Objects.isNull(storyResponse)) {
            throw new ClientException(Constant.MESSAGE_ERROR_CLIENT);
        }else{
            String response = storyResponse.getHits().stream()
                    .map(item -> createStory(getStoryService().findStoryById(item.getStory_id()), item))
                    .collect(Collectors.joining(","));
            log.info("DATA: "+response);
        }
    }

    private String createStory(StoryDto storyDto, DataStory response){
        try {
            if (Objects.isNull(storyDto) && Objects.nonNull(response.getStory_id())) {
                ObjectMapper mapper = new ObjectMapper();
                StoryDto storyDtoFactory = getStoryFactory().create(
                        response.getStory_id(),
                        response.getCreated_at(),
                        response.getTitle().isPresent() ? response.getTitle().get() : "",
                        response.getUrl().isPresent() ? response.getUrl().get() : "",
                        response.getAuthor(),
                        response.getPoints().isPresent() ? response.getPoints().get() : "",
                        response.getStory_text().isPresent() ? response.getStory_text().get() : "",
                        response.getComment_text(),
                        response.getNum_comments().isPresent() ? response.getNum_comments().get() : null,
                        response.getStory_title(),
                        response.getStory_url(),
                        response.getParent_id(),
                        response.getCreated_at_i(),
                        response.get_tags(),
                        response.getObjectID(),
                        mapper.writeValueAsString(response.get_highlightResult()),
                        true);
                if (getStoryService().saveStory(storyDtoFactory)) {
                    return Constant.MESSAGE_SAVE + response.getStory_id();
                }
                return Constant.MESSAGE_ERROR_SAVE + response.getStory_id();
            }
            return Constant.MESSAGE_ALREADY_REGISTERED + response.getStory_id();
        }catch (Exception ex){
            return Constant.MESSAGE_ERROR_JSON_CONVERT + response.getStory_id();
        }
    }
}
