/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.application.mapper;

import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.model.response.news.story.DataStory;

public class StoryResponseMapperToStoryDto {

    public StoryDto mapper(DataStory response){
        return new StoryDto(
                response.getStory_id(),
                response.getCreated_at(),
                response.getTitle().isPresent()? response.getTitle().get(): "",
                response.getUrl().isPresent()? response.getUrl().get(): "",
                response.getAuthor(),
                response.getPoints().isPresent()? response.getPoints().get(): "",
                response.getStory_text().isPresent()? response.getStory_text().get(): "",
                response.getComment_text(),
                response.getNum_comments().isPresent()? response.getNum_comments().get(): null,
                response.getStory_title(),
                response.getStory_url(),
                response.getParent_id(),
                response.getCreated_at_i(),
                response.get_tags(),
                response.getObjectID(),
                response.get_highlightResult().toString(),
                true
        );

    }
}
