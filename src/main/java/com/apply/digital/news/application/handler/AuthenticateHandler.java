/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.application.handler;

import com.apply.digital.news.domain.model.dto.UserDto;
import com.apply.digital.news.domain.port.security.TokenSecurity;

public class AuthenticateHandler {

    private final TokenSecurity tokenSecurity;

    public AuthenticateHandler(TokenSecurity tokenSecurity) {
        this.tokenSecurity = tokenSecurity;
    }

    private TokenSecurity getTokenSecurity(){
        return tokenSecurity;
    }

    public UserDto validateUser(String username, String password){

        UserDto user = new UserDto();
        if(username != null && !username.isEmpty() && password != null && !password.isEmpty()) {
            user.setUsername(username);
            user.setToken(getTokenSecurity().generateToken(username));
        }else{
            user = null;
        }
        return user;
    }
}
