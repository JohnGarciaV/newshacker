/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.application.exception;

public class MonthValueException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public MonthValueException(String message) {
        super(message);
    }
}
