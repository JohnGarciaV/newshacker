/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.application.handler;

import com.apply.digital.news.application.constant.Constant;
import com.apply.digital.news.application.exception.MonthValueException;
import com.apply.digital.news.application.factory.ConditionsFactory;
import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.model.response.data.StoryResponse;
import com.apply.digital.news.domain.service.StoryService;
import org.springframework.data.domain.PageRequest;

import java.util.HashMap;
import java.util.Objects;

public class StoryHandler {

    private final StoryService storyService;
    private final ConditionsFactory conditionsFactory;

    public StoryHandler(StoryService storyService, ConditionsFactory conditionsFactory) {
        this.storyService = storyService;
        this.conditionsFactory = conditionsFactory;
    }

    private StoryService getStoryService(){
        return storyService;
    }


    private ConditionsFactory getConditionsFactory(){
        return conditionsFactory;
    }

    public StoryDto deleteStory(Long story_id){
        StoryDto story = getStoryService().findStoryById(story_id);
        if(Objects.nonNull(story)){
            story.setState(false);
            if (getStoryService().saveStory(story)) {
                return story;
            }
        }
        return null;
    }

    public StoryResponse findAllStoryByFilter(String author, String tags, String title, String month, int page){
        try {
            HashMap<String, Object> conditions = getConditionsFactory().getConditions(author, tags, title, month);
            return getStoryService().findByFilters(conditions, PageRequest.of(page, Constant.ELEMENTS));
        }catch (MonthValueException ex){
            return new StoryResponse();
        }
    }
}
