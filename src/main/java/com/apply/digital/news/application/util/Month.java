package com.apply.digital.news.application.util;

public enum Month {
    ENERO("ENERO", "JANUARY"),
    FEBRERO("FEBRERO", "FEBRUARY"),
    MARZO("MARZO", "MARCH"),
    ABRIL("ABRIL", "APRIL"),
    MAYO("MAYO", "MAY"),
    JUNIO("JUNIO", "JUNE"),
    JULIO("JULIO", "JULY"),
    AGOSTO("AGOSTO", "AUGUST"),
    SEPTIEMBRE("SEPTIEMBRE", "SEPTEMBER"),
    OCTUBRE("OCTUBRE", "OCTOBER"),
    NOVIEMBRE("NOVIEMBRE", "NOVEMBER"),
    DICIEMBRE("DICIEMBRE", "DECEMBER");


    private final String nameSpanish;
    private final String nameEnglish;

    Month(String nameSpanish, String nameEnglish) {
        this.nameSpanish = nameSpanish;
        this.nameEnglish = nameEnglish;
    }

    public static String findByNameSpanish(String months) {
        for (Month month : values()) {
            if (month.nameSpanish.equals(months)) {
                return month.nameEnglish;
            }
        }
        return null;
    }

    public static boolean isMonth(String months) {
        for (Month month : values()) {
            if (month.nameSpanish.equals(months)) {
                return true;
            }
        }
        return false;
    }
}
