package com.apply.digital.news.application.exception;

public class ClientException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ClientException(String message) {
        super(message);
    }
}
