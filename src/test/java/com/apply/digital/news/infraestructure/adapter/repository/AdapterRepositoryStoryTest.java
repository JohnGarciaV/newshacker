/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.infraestructure.adapter.repository;

import com.apply.digital.news.TestValue;
import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.model.response.data.StoryResponse;
import com.apply.digital.news.domain.model.response.news.story.DataStory;
import com.apply.digital.news.infraestructure.adapter.entity.Story;
import com.apply.digital.news.infraestructure.adapter.mapper.MapperToStoryResponseDeserializeField;
import com.apply.digital.news.infraestructure.adapter.mapper.StoryDtoMapperToStory;
import com.apply.digital.news.infraestructure.adapter.mapper.StoryMapperToStoryDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class AdapterRepositoryStoryTest {

    @Spy
    @InjectMocks
    private AdapterRepositoryStory adapterRepositoryStory;

    @Mock
    private StoryDao storyDao;

    @Mock
    private StoryDtoMapperToStory storyDtoMapperToStory;

    @Mock
    private StoryMapperToStoryDto storyMapperToStoryDto;

    @Mock
    private StoryRepositoryImpl storyRepositoryImpl;

    @Mock
    private MapperToStoryResponseDeserializeField mapperDeserializeField;

    @Test
    void whenSaveStoryThenReturnTrue() {
        //Given
        StoryDto storyDto = TestValue.buildResponseStoryDto();
        Story story = TestValue.buildStory();
        //when
        when(storyDtoMapperToStory.mapper(any()))
                .thenReturn(story);
        when(storyDao.save(Mockito.any(Story.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        boolean result = this.adapterRepositoryStory.saveStory(storyDto);
        //Then
        Assertions.assertTrue(result);
    }

    @Test
    void whenSaveStoryMapperThenReturnFalse() {
        //Given
        StoryDto storyDto = TestValue.buildResponseStoryDto();
        //when
        when(storyDtoMapperToStory.mapper(any()))
                .thenReturn(null);
        when(storyDao.save(Mockito.any(Story.class)))
                .thenThrow(RuntimeException.class);

        boolean result = this.adapterRepositoryStory.saveStory(storyDto);
        //Then
        Assertions.assertFalse(result);
    }

    @Test
    void whenStoryFindByIdThenReturnStoryDto() {
        //Given
        StoryDto storyDto = TestValue.buildResponseStoryDto();
        Story story = TestValue.buildStory();
        //when
        when(storyDao.findById(anyLong()))
                .thenReturn(Optional.of(story));
        when(storyMapperToStoryDto.mapper(any()))
                .thenReturn(storyDto);

        StoryDto result = this.adapterRepositoryStory.findById(1L);
        //Then
        Assertions.assertNotNull(result);
        Assertions.assertEquals("1", result.getPoints());
        Assertions.assertEquals(1L, result.getNumComments());
        Assertions.assertEquals(90061L, result.getCreatedAtI());
        Assertions.assertEquals(Arrays.asList("comment","software"), result.getTags());
        Assertions.assertEquals("", result.getHighlightResult());
    }

    @Test
    void whenStoryFindByIdNotFoundThenReturnNull() {
        //Given
        //when
        when(storyDao.findById(anyLong()))
                .thenReturn(Optional.empty());

        StoryDto result = this.adapterRepositoryStory.findById(1L);
        //Then
        Assertions.assertNull(result);
    }

    @Test
    void whenStoryFindByIdNullThenReturnException() {
        //Given
        //when
        when(storyDao.findById(anyLong()))
                .thenThrow(RuntimeException.class);

        StoryDto result = this.adapterRepositoryStory.findById(1L);
        //Then
        Assertions.assertNull(result);
    }

    @Test
    void whenStoryFindByAllFiltersThenReturnStoryResponseList() {
        //Given
        Story story = TestValue.buildStory();
        List<Story> listStories = Arrays.asList(story);
        Pageable pageable = PageRequest.of(0, 5);
        Page<Story> pageStory = new PageImpl<>(listStories, pageable, 1);
        DataStory dataStory = TestValue.buildResponseDataStory();
        HashMap<String, Object> filters = TestValue.buildFilters();
        List<DataStory> listDataStory = Arrays.asList(dataStory);
        //when
        when(storyRepositoryImpl.getDataFilterAndPagination(any(), any()))
                .thenReturn(pageStory);
        when(mapperDeserializeField.mapper(any()))
                .thenReturn(dataStory);

        StoryResponse result = this.adapterRepositoryStory.findByFilters(filters, pageable);
        //Then
        Assertions.assertNotNull(result);
        Assertions.assertEquals(listDataStory, result.getStories());
    }

    @Test
    void whenStoryFindByFiltersWithoutCreatedAtThenReturnStoryResponseList() {
        //Given
        Story story = TestValue.buildStory();
        List<Story> listStories = Arrays.asList(story);
        Pageable pageable = PageRequest.of(0, 5);
        Page<Story> pageStory = new PageImpl<>(listStories, pageable, 1);
        DataStory dataStory = TestValue.buildResponseDataStory();
        HashMap<String, Object> filters = TestValue.buildFiltersWithoutCreatedAt();
        List<DataStory> listDataStory = Arrays.asList(dataStory);
        //when
        when(storyRepositoryImpl.getDataFilterAndPagination(any(), any()))
                .thenReturn(pageStory);
        when(mapperDeserializeField.mapper(any()))
                .thenReturn(dataStory);

        StoryResponse result = this.adapterRepositoryStory.findByFilters(filters, pageable);
        //Then
        Assertions.assertNotNull(result);
        Assertions.assertEquals(listDataStory, result.getStories());
    }

    @Test
    void whenStoryFindByFiltersThenReturnStoryResponseListEmpty() {
        //Given
        Pageable pageable = PageRequest.of(0, 5);
        HashMap<String, Object> filters = TestValue.buildFilters();
        //when
        when(storyRepositoryImpl.getDataFilterAndPagination(any(), any()))
                .thenReturn(null);

        StoryResponse result = this.adapterRepositoryStory.findByFilters(filters, pageable);
        //Then
        Assertions.assertNotNull(result);
        Assertions.assertEquals(0, result.getSize());
        Assertions.assertEquals(0, result.getTotalElements());
    }

}
