/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.service;

import com.apply.digital.news.TestValue;
import com.apply.digital.news.domain.model.response.news.story.Stories;
import com.apply.digital.news.domain.port.consumer.NewsHackerConsumer;
import com.apply.digital.news.domain.service.NewsHackerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.TestPropertySource;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class NewsHackerServiceTest {

    @Spy
    @InjectMocks
    private NewsHackerService newsHackerService;

    @Mock
    private NewsHackerConsumer newsHackerConsumer;

    @Test
    void saveStoryWhenGetNewStoriesThenStatusOk() {
        //Given
        Stories response = TestValue.buildResponseStories();
        //when
        when(newsHackerConsumer.getNewsStories())
                .thenReturn(response);
        //Then
        Stories result = this.newsHackerService.execute();
        Assertions.assertNotNull(result);
        Assertions.assertNotNull(result.getHits());
        Assertions.assertEquals(1, result.getPage());
        Assertions.assertEquals(1, result.getNbHits());
        Assertions.assertEquals(200, result.getProcessingTimeMS());
        Assertions.assertEquals(700, result.getServerTimeMS());
    }

    @Test
    void saveStoryWhenGetNewStoriesThenStatusFailed() {
        //Given
        //when
        when(newsHackerConsumer.getNewsStories())
                .thenReturn(null);
        //Then
        Stories result = this.newsHackerService.execute();
        Assertions.assertNull(result);
    }
}
