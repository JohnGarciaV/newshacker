/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.service;

import com.apply.digital.news.TestValue;
import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.model.response.data.StoryResponse;
import com.apply.digital.news.domain.port.repository.StoryRepository;
import com.apply.digital.news.domain.service.StoryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.TestPropertySource;

import java.util.HashMap;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class StoryServiceTest {

    @Spy
    @InjectMocks
    private StoryService storyService;

    @Mock
    private StoryRepository storyRepository;

    @Test
    void whenSaveStoryThenReturnTrue() {
        //Given
        StoryDto storyDto = TestValue.buildResponseStoryDto();
        //when
        when(storyRepository.saveStory(storyDto))
                .thenReturn(true);
        //Then
        boolean result = this.storyService.saveStory(storyDto);
        Assertions.assertTrue(result);
    }

    @Test
    void findStoryByIdWhenThenReturnStoryDtoOk() {
        //Given
        StoryDto storyDto = TestValue.buildResponseStoryDto();
        //when
        when(storyRepository.findById(1L))
                .thenReturn(storyDto);
        //Then
        StoryDto result = this.storyService.findStoryById(1L);
        Assertions.assertEquals("author", result.getAuthor());
        Assertions.assertEquals("url", result.getUrl());
        Assertions.assertEquals("test", result.getTitle());
        Assertions.assertEquals("text", result.getStoryText());
        Assertions.assertEquals("The comment", result.getCommentText());
        Assertions.assertEquals("The tittle story", result.getStoryTitle());
        Assertions.assertEquals("The url story", result.getStoryUrl());
        Assertions.assertEquals(1L, result.getParentId());
        Assertions.assertEquals("2023-03-18T10:36:56.000Z", result.getCreatedAt());
        Assertions.assertEquals("35181958", result.getObjectID());
    }

    @Test
    void findByFiltersWhenThenReturnStoryResponseOk() {
        //Given
        StoryResponse storyResponse = TestValue.buildStoryResponse();
        HashMap<String, Object> filters =  TestValue.buildFilters();
        //when
        when(storyRepository.findByFilters(filters, PageRequest.of(0, 5)))
                .thenReturn(storyResponse);
        //Then
        StoryResponse result = this.storyService.findByFilters(filters, PageRequest.of(0, 5));
        Assertions.assertEquals(0, result.getTotalPages());
        Assertions.assertEquals(1, result.getTotalElements());
        Assertions.assertEquals(1, result.getSize());
    }

}
