/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.handler;

import com.apply.digital.news.TestValue;
import com.apply.digital.news.application.factory.StoryFactory;
import com.apply.digital.news.application.handler.NewsHackerHandler;
import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.model.response.news.story.Stories;
import com.apply.digital.news.domain.service.NewsHackerService;
import com.apply.digital.news.domain.service.StoryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.TestPropertySource;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class NewsHackerHandlerTest {

    @Spy
    @InjectMocks
    private NewsHackerHandler newsHackerHandler;

    @Mock
    private NewsHackerService newsHackerService;

    @Mock
    private StoryService storyService;

    @Mock
    private StoryFactory storyFactory;

    @Test
    void whenNewsHackerHandlerThenReturnLog() {
        //Given
        Stories stories = TestValue.buildResponseStories();
        StoryDto storyDto = TestValue.buildResponseStoryDto();
        //When
        when(newsHackerService.execute())
                .thenReturn(stories);
        when(storyService.findStoryById(anyLong()))
                .thenReturn(storyDto);

         this.newsHackerHandler.execute();
        //Then
        verify(newsHackerHandler).execute();
    }

    @Test
    void whenNewsHackerHandlerThenReturnThrowException() {
        //Given
        //when
        try {
            this.newsHackerHandler.execute();
        }catch (RuntimeException ex) {
        //Then
            verify(newsHackerHandler).execute();
        }
    }

     /*Awaitility.await()
                .atMost(Duration.of(1500, ChronoUnit.MILLIS))
                .untilAsserted(() ->
                        verify(newsHackerHandler, atLeast(1)).execute()
         );*/
}
