/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.handler;

import com.apply.digital.news.TestValue;
import com.apply.digital.news.application.handler.AuthenticateHandler;
import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.model.dto.UserDto;
import com.apply.digital.news.domain.port.security.TokenSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.TestPropertySource;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class AuthenticateHandlerTest {

    @Spy
    @InjectMocks
    private AuthenticateHandler authenticateHandler;

    @Mock
    private TokenSecurity tokenSecurity;

    @Test
    void whenValidateUserThenReturnUserDto() {
        //Given
        UserDto storyDto = TestValue.buildUserDto();
        //when
        when(tokenSecurity.generateToken(anyString()))
                .thenReturn("token");
        //Then
        UserDto result = this.authenticateHandler.validateUser("user", "pwd");
        Assertions.assertEquals("user", result.getUsername());
        Assertions.assertEquals("token", result.getToken());
    }

    @Test
    void whenUsernameEmptyThenReturnNull() {
        //Given
        //when
        UserDto result = this.authenticateHandler.validateUser("", "pwd");
        //Then
        Assertions.assertNull(result);
    }

    @Test
    void whenPasswordEmptyThenReturnNull() {
        //Given
        //when
        UserDto result = this.authenticateHandler.validateUser("user", "");
        //Then
        Assertions.assertNull(result);
    }

    @Test
    void whenUsernameIsNullThenReturnNull() {
        //Given
        //when
        UserDto result = this.authenticateHandler.validateUser(null, "pwd");
        //Then
        Assertions.assertNull(result);
    }

    @Test
    void whenPasswordIsNullThenReturnNull() {
        //Given
        //when
        UserDto result = this.authenticateHandler.validateUser("user", null);
        //Then
        Assertions.assertNull(result);
    }
}
