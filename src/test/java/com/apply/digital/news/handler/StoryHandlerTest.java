/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news.handler;

import com.apply.digital.news.TestValue;
import com.apply.digital.news.application.factory.ConditionsFactory;
import com.apply.digital.news.application.factory.StoryFactory;
import com.apply.digital.news.application.handler.StoryHandler;
import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.model.response.data.StoryResponse;
import com.apply.digital.news.domain.service.StoryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.TestPropertySource;

import java.util.HashMap;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class StoryHandlerTest {

    @Spy
    @InjectMocks
    private StoryHandler StoryHandler;

    @Mock
    private StoryService storyService;

    @Mock
    private StoryFactory storyFactory;

    @Mock
    private ConditionsFactory conditionsFactory;

    @Test
    void whenStoryHandlerThenReturnStoryDtoOK() {
        //Given
        StoryDto storyDto = TestValue.buildResponseStoryDto();
        //when
        when(storyService.findStoryById(anyLong()))
                .thenReturn(storyDto);

        when(storyService.saveStory(any()))
                .thenReturn(true);

        StoryDto response = this.StoryHandler.deleteStory(1L);
        //Then
        Assertions.assertEquals(1L, response.getStoryId());
        Assertions.assertEquals(false, response.isState());
        verify(storyService).findStoryById(anyLong());
        verify(storyService).saveStory(any());
    }

    @Test
    void whenStoryHandlerSaveStoryThenReturnNull() {
        //Given
        StoryDto storyDto = TestValue.buildResponseStoryDto();
        //when
        when(storyService.findStoryById(anyLong()))
                .thenReturn(storyDto);

        when(storyService.saveStory(any()))
                .thenReturn(false);

        StoryDto response = this.StoryHandler.deleteStory(1L);
        //Then
        Assertions.assertNull(response);
    }

    @Test
    void whenStoryHandlerFindStoryByIdThenReturnNull() {
        //Given
        //when
        when(storyService.findStoryById(anyLong()))
                .thenReturn(null);
        StoryDto response = this.StoryHandler.deleteStory(1L);
        //Then
        Assertions.assertNull(response);
    }

    @Test
    void whenStoryHandlerFindStoryByIdThenReturnStoryResponse() {
        //Given
        HashMap<String, Object> filters = TestValue.buildFilters();
        StoryResponse storyResponse = TestValue.buildStoryResponse();
        //when
        when(conditionsFactory.getConditions(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(filters);
        when(storyService.findByFilters(any(), any()))
                .thenReturn(storyResponse);

        StoryResponse result = this.StoryHandler.findAllStoryByFilter("author", "comment", "test", "MARZO", 0);
        //Then
        Assertions.assertNotNull(result);
        Assertions.assertEquals(0, result.getTotalPages());
        Assertions.assertEquals(1, result.getPageCurrent());
        Assertions.assertEquals(1, result.getTotalElements());
        Assertions.assertEquals(1, result.getSize());
    }

}
