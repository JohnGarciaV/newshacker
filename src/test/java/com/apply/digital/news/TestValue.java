/**
 * Apply Digital
 *
 * <p>Copyright (c) 2022 . All Rights Reserved.
 *
 * <p>NOTICE: This file is subject to the terms and conditions defined in file 'LICENSE', which is
 * part of this source code package.
 */
package com.apply.digital.news;

import com.apply.digital.news.domain.model.dto.StoryDto;
import com.apply.digital.news.domain.model.dto.UserDto;
import com.apply.digital.news.domain.model.response.data.StoryResponse;
import com.apply.digital.news.domain.model.response.news.story.DataStory;
import com.apply.digital.news.domain.model.response.news.story.Stories;
import com.apply.digital.news.infraestructure.adapter.entity.Story;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;

public class TestValue {

    public static Stories buildResponseStories() {
        DataStory dataStory =buildResponseDataStory();
        Stories stories = new Stories();
        stories.setHits(Arrays.asList(dataStory));
        stories.setNbHits(1);
        stories.setPage(1);
        stories.setHitsPerPage(1);
        stories.setExhaustiveTypo(false);
        stories.setExhaustiveNbHits(true);
        stories.setExhaustiveT(null);
        stories.setParams("Java");
        stories.setQuery("");
        stories.setProcessingTimeMS(200);
        stories.setProcessingTimingsMS(null);
        stories.setServerTimeMS(700);
        return stories;
    }

    public static DataStory buildResponseDataStory() {
        DataStory dataStory = new DataStory();
        dataStory.setCreated_at("2023-03-18T10:36:56.000Z");
        dataStory.setTitle(Optional.of("test"));
        dataStory.setUrl(Optional.of("url"));
        dataStory.setAuthor("author");
        dataStory.setPoints(Optional.of("1"));
        dataStory.setStory_text(Optional.of("story text"));
        dataStory.setComment_text("comment text");
        dataStory.setNum_comments(Optional.of(0L));
        dataStory.setStory_id(0);
        dataStory.setStory_title("story title");
        dataStory.setStory_url("story url");
        dataStory.setParent_id(0);
        dataStory.setCreated_at_i(0);
        dataStory.set_tags(Arrays.asList("comment"));
        dataStory.setObjectID("Object");
        dataStory.set_highlightResult(null);
        return dataStory;
    }

    public static StoryDto buildResponseStoryDto() {
        return new StoryDto(
            1L,
                "2023-03-18T10:36:56.000Z",
                "test",
                "url",
                "author",
                "1",
                "text",
                "The comment",
                1L,
                "The tittle story",
                "The url story",
                1L,
                90061L,
                Arrays.asList("comment","software"),
                "35181958",
                "",
                true
        );
    }

    public static StoryResponse buildStoryResponse() {
        StoryResponse storyResponse = new StoryResponse();
        storyResponse.setSize(1);
        storyResponse.setPageCurrent(1);
        storyResponse.setTotalElements(1);
        return storyResponse;
    }

    public static HashMap<String, Object> buildFilters() {
        HashMap<String, Object> filters = new HashMap<>();
        filters.put("author", "author");
        filters.put("tags", "comment");
        filters.put("title", "test");
        filters.put("created_at", "MARCH");
        return filters;
    }

    public static HashMap<String, Object> buildFiltersWithoutCreatedAt() {
        HashMap<String, Object> filters = new HashMap<>();
        filters.put("author", "author");
        filters.put("tags", "comment");
        filters.put("title", "test");
        return filters;
    }

    public static UserDto buildUserDto() {
        UserDto userDto = new UserDto();
        userDto.setUsername("user");
        userDto.setToken("token");
        return userDto;
    }

    public static Story buildStory() {
        Story story = new Story();
        story.setStoryId(1L);
        story.setCreatedAt("2023-03-18T10:36:56.000Z");
        story.setTitle("test");
        story.setUrl("url");
        story.setAuthor("author");
        story.setPoints("1");
        story.setStoryText("story text");
        story.setCommentText("comment text");
        story.setNumComments(2L);
        story.setStoryTitle("story tittle");
        story.setStoryUrl("story url");
        story.setParentId(1L);
        story.setCreatedAtI(9876523L);
        story.setTags(Arrays.asList("comment","software"));
        story.setObjectID("object");
        story.setHighlightResult("high");
        story.setState(true);
        return story;
    }
}
