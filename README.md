# newshacker

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/johngarcia1/newshacker.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/johngarcia1/newshacker/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Tencnologías a usadas:
- Java version: 11
- Gradle version: 7.6.1
- Spring Boot version: v2.7.4.RELEASE
- Postgresql version 42.3.7
- Docker

## Arquitectura usada
- Hexagonal

## Migration Data
Si se desea ingresar datos para pruebas crear archivo `data.sql` en el directorio `main/resources` en el archivo `application.properties` habilitar `spring.sql.init.mode=always` ingresar información ejemplo:
```
INSERT INTO story ( story_id, created_at, title, url, author, points, story_text, comment_text, num_comments,
story_title, story_url, parent_id, created_at_i, tags, object_id, high_light_result, state)
VALUES( 1, '2023-03-18T10:36:56.000Z', 'test', 'url', 'author', '1', 'text', 'The comment', 1, 'The tittle story',
'The url story', 1, 90061, '{"comment","software"}', '35181958', '', true);
```

## Name
news Hacker story

## Description
The api allows to get new stories every hour and store them in a database, you can query these stories and filter them, you can delete a story, in order to use the application you must obtain a token.


## Installation
 - prerequisites
	-Docker

Make sure docker is running, go to the root of the project where the Dockerfile and docker-compose.yml files are located.

Run the following command to create a docker image from a terminal:
 docker build -t hacker_news-0.0.1.jar .

Once the process is finished, run the following command to run the multi-containers where the database server and api are located:
  docker-compose up -d
 

## Usage
First get token, you don't need to be registered, example:

```
localhost:8000/story/login?username=test&password=123
```

Get story by filters, select the filters of your choice, filters, author, tags, tittle, month.

```
localhost:8000/story/filter/0?tags=comment&title=test&author=Lazare&month=MRS
```

Delete story by id story

```
localhost:8000/story/1
```


If you have more questions about its use, consult the documentation.

```
http://localhost:8000//api-docs
```


## Authors
John Anderson Garcia Vanegas

## License
<p>Copyright (c) 2022 . All Rights Reserved. Apply Digital

## Project status
Completed