FROM openjdk:11
ADD build/libs/hacker_news-0.0.1.jar hacker_news-0.0.1.jar
ENTRYPOINT ["java", "-jar","hacker_news-0.0.1.jar"]
EXPOSE 8000